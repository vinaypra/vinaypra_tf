#!/usr/bin/env python  
import rospy
import tf
import math
import time

if __name__ == '__main__':
    rospy.init_node('my_moving_carrot_tf_broadcaster')
    br = tf.TransformBroadcaster()
    rate = rospy.Rate(5.0)
    turning_speed_rate = 0.1

    # Modify later to use shutdownhook() function
    while not rospy.is_shutdown():
        t = (rospy.Time.now().to_sec() * math.pi)*turning_speed_rate
        
        # Map to only one turn maximum [0,2*pi)
        rad_var = t % (2*math.pi)

        # Converting from Euler to Quaternion
        quatern = tf.transformations.quaternion_from_euler(0.0,0.0,rad_var)

        quatern_0 = quatern[0]
        quatern_1 = quatern[1]
        quatern_2 = quatern[2]
        quatern_3 = quatern[3]

        br.sendTransform((1.0, 0.0, 0.0),
                         (quatern_0, quatern_1, quatern_2, quatern_3),
                         rospy.Time.now(),
                         "moving_carrot",
                         "coke_can") # Plugged in quaternion values and set referred frame as coke_can frame
        rate.sleep()