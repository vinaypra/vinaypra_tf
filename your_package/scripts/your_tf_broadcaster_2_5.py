#! /usr/bin/env python
import rospy
import time
import tf
import math
from turtle_tf_3d.get_model_gazebo_pose import GazeboModel


def handle_turtle_pose(pose_msg, robot_name):
    br = tf.TransformBroadcaster()
    '''
    br.sendTransform((pose_msg.position.x,pose_msg.position.y,pose_msg.position.z),
                     (pose_msg.orientation.x,pose_msg.orientation.y,pose_msg.orientation.z,pose_msg.orientation.w),
                     rospy.Time.now(),
                     robot_name,
                     "/fixed_carrot")
    '''
    #'''
    br.sendTransform((pose_msg.position.x,pose_msg.position.y,pose_msg.position.z),
                     (pose_msg.orientation.x,pose_msg.orientation.y,pose_msg.orientation.z,pose_msg.orientation.w),
                     rospy.Time.now(),
                     robot_name,
                     "/moving_carrot")
    #'''

def publisher_of_tf():
    
    #rospy.init_node('publisher_of_tf_node', anonymous=True)
    robot_name_list = ["turtle1","turtle2"]
    gazebo_model_object = GazeboModel(robot_name_list)
    
    # Leave enough time to be sure the Gazebo Model logs have finished
    time.sleep(1)
    rospy.loginfo("Ready..Starting to Publish TF data now...")
    
    rate = rospy.Rate(5) # 5hz
    while not rospy.is_shutdown():
        for robot_name in robot_name_list:
            pose_now = gazebo_model_object.get_model_pose(robot_name)
            if not pose_now:
                print "The " + str(robot_name) + "'s Pose is not yet available...Please try again later"
            else:
                handle_turtle_pose(pose_now, robot_name)
        rate.sleep()
    

if __name__ == '__main__':
    '''
    rospy.init_node('my_fixed_carrot_tf_broadcaster')
    br_1 = tf.TransformBroadcaster()
    rate = rospy.Rate(3.0)

    while not rospy.is_shutdown():
        br_1.sendTransform((1.0, 0.0, 3.0),
                         (0.0, 0.0, 0.0, 1.0),
                         rospy.Time.now(),
                         "fixed_carrot",
                         "turtle2") #changed for iRobot

        publisher_of_tf()
        rate.sleep()
    '''
    #'''
    rospy.init_node('my_moving_carrot_tf_broadcaster')
    br_1 = tf.TransformBroadcaster()
    rate = rospy.Rate(5.0)
    turning_speed_rate = 0.1

    # Modify later to use shutdownhook() function
    while not rospy.is_shutdown():
        t = (rospy.Time.now().to_sec() * math.pi)*turning_speed_rate
        
        # Map to only one turn maximum [0,2*pi)
        rad_var = t % (2*math.pi)

        # Converting from Euler to Quaternion
        quatern = tf.transformations.quaternion_from_euler(0.0,0.0,rad_var)

        quatern_0 = quatern[0]
        quatern_1 = quatern[1]
        quatern_2 = quatern[2]
        quatern_3 = quatern[3]

        br_1.sendTransform((1.0, 0.0, 0.0),
                         (quatern_0, quatern_1, quatern_2, quatern_3),
                         rospy.Time.now(),
                         "moving_carrot",
                         "turtle2") # Plugged in quaternion values and set referred frame as coke_can frame
        publisher_of_tf()
        rate.sleep()
    #'''    
    '''
    try:
        publisher_of_tf()
    except rospy.ROSInterruptException:
        pass
    '''